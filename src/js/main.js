var lazyLoadInstance = new LazyLoad({
    elements_selector: ".lazy"
});

//smooth scroll
$(document).ready(function(){
    $("a.link").click(function( event ) {
        event.preventDefault();
        $("html, body").animate({ scrollTop: $($(this).attr("href")).offset().top}, 500);
    });
});

// mobile nav button & menu
$(document).ready(function(){
    var nav = $(".nav-cont")
    var hamburgers = document.querySelectorAll(".hamburger");
    var forEach=function(t,o,r){if("[object Object]"===Object.prototype.toString.call(t))for(var c in t)Object.prototype.hasOwnProperty.call(t,c)&&o.call(r,t[c],c,t);else for(var e=0,l=t.length;l>e;e++)o.call(r,t[e],e,t)};

    $("#btn").click(function() {
        nav.toggle();
        nav.addClass("active");
        $("#links").removeClass("uk-animation-scale-up")
        $("#links").addClass("uk-animation-slide-right")
    });

    $("a.link").click(function() {
        $(".active").hide();
        nav.removeClass("active");
        $("#btn").removeClass("is-active")
    });

    if (hamburgers.length > 0) {
        forEach(hamburgers, function(hamburger) {
            hamburger.addEventListener("click", function() {
            this.classList.toggle("is-active");
            }, false);
        });
        }
});

// simple light box

$(document).ready(function(){
    if ($('.gallery')[0]){
        alert('gallery exists on the page..');
        var gallery = $('.gallery a').simpleLightbox();
    }   
});




if( window.location.href.indexOf('?registered=completed') !== -1 ){
    UIkit.notification({
        message: '<span uk-icon="icon: check"></span> Thank you for registering successfully. Welcome to the Members Area.',
        timeout: 10000,
        status: 'success'
       // status:'primary'
    });
}

