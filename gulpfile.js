"use strict";

const { dest, pipe, series, src, parallel, watch } = require("gulp");

const uglify = require("gulp-uglify");
const pump = require("pump");
const rename = require("gulp-rename");
const babel = require("gulp-babel");
const pug = require("gulp-pug");
const sass = require("gulp-sass");
const autoprefixer = require("gulp-autoprefixer");
const jade = require("gulp-jade");
const bs = require("browser-sync").create();

const enableBrowserSync = true; // | false
let browserSyncInitialised = false;

function browserSyncInit() {
  if (enableBrowserSync) {
    bs.init({
      proxy: "http://localhost/jado/web"
    });
    browserSyncInitialised = true;
  }
}

function browserSyncReload(done) {
  bs.reload();
  done();
}

function compileJs(cb) {
  pump(
    [
      src("src/js/**/*.js"),
      babel({
        presets: ["@babel/preset-env"]
      }),
      uglify(),
      rename({ suffix: ".min" }),
      dest("web/build/js")
    ],
    cb
  );
}

function compilePugTemplates() {
  return src("templates/**/*.pug")
    .pipe(
      pug({
        pretty: false
      })
    )
    .pipe(dest("templates/"));
}

function compileJadeTemplates() {
  return src("templates/**/*.jade")
    .pipe(
      jade({
        pretty: false
      })
    )
    .on("error", function(err) {
      console.log(err.toString());

      this.emit("end");
    })

    .pipe(dest("templates/"));
}

function transpileCss() {
  return src("src/sass/*.scss")
    .pipe(
      autoprefixer({
        browsers: ["last 3 versions"],
        cascade: false
      })
    )
    .pipe(sass({ outputStyle: "compressed" }).on("error", sass.logError))
    .pipe(rename({ suffix: ".min" }))
    .pipe(dest("web/build/css"));
}

function watchFiles() {
  watch("src/sass/**/*.scss", transpileCss);
  watch("src/js/**/*.js", compileJs);
  

  watch("templates/**/*.pug", series(compilePugTemplates, browserSyncReload))

  watch("templates/**/*.jade", series(compileJadeTemplates, browserSyncReload))

//   watch("templates/**/*.pug", () => {
//     if (browserSyncInitialised) {
//       series(compilePugTemplates, browserSyncReload);
//       return;
//     }
//     compilePugTemplates()
//   })

//   watch("templates/**/*.jade", () => {
//     if (browserSyncInitialised) {
//       series(compileJadeTemplates, browserSyncReload);
//       return;
//     }
//     compileJadeTemplates();
//   })
}

exports.compileJs = series(compileJs);
exports.compilePugTemplates = series(compilePugTemplates);
exports.compileJadeTemplates = series(compileJadeTemplates);
exports.buildSass = series(transpileCss);
exports.watchFiles = series(watchFiles);

exports.default = series(
  parallel(compileJs, compilePugTemplates, compileJadeTemplates, transpileCss),
  watchFiles
);

exports.dev = series(
  parallel(compileJs, compilePugTemplates, compileJadeTemplates, transpileCss),
  parallel(browserSyncInit,  watchFiles)
);