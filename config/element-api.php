<?php

use craft\elements\Entry;
use craft\helpers\UrlHelper;
use aelvan\imager\variables\ImagerVariable;

$purchaseType = Craft::$app->request->getQueryParam('purchaseType');
$minPrice = Craft::$app->request->getQueryParam('minPrice');
$maxPrice = Craft::$app->request->getQueryParam('maxPrice');
$showAll = Craft::$app->request->getQueryParam('showAll');
$bedrooms =  Craft::$app->request->getQueryParam('bedrooms');
$residentialValue = Craft::$app->request->getQueryParam('residentialValue');
$parish = Craft::$app->request->getQueryParam('parish');
$order =  Craft::$app->request->getQueryParam('order');
$propertySection = Craft::$app->request->getQueryParam('propertySection');

$criteria = [
    'section' => $propertySection,
    'orderBy' => $order
];

//craft\elements\Category::find()->id($purchaseType)->first()

//Related to filterings for category fields
//All three (purchase type, residential value and a parish value)
if ($purchaseType && $residentialValue && $parish) {
    $criteria['relatedTo'] = [
        'and',
        ['element' => $purchaseType],
        ['element' => $residentialValue],
        ['element' => explode( ',' , $parish )],
    ];
//Just purchase type plus the other two
} elseif ($purchaseType && $residentialValue) {
    $criteria['relatedTo'] = [
        'and',
        ['element' => $purchaseType],
        ['element' => $residentialValue],
    ];
} elseif ($purchaseType && $parish ) {
    $criteria['relatedTo'] = [
        'and',
        ['element' => $purchaseType],
        ['element' => explode( ',' , $parish )],
    ];
//Just a purchase type
} elseif ($purchaseType) {
    $criteria['relatedTo'] = [
        ['element' => $purchaseType],

    ];
} 


//price is defined below as entry-> price. This takes the price field attached to the entry
if($minPrice && $maxPrice) {
    //$criteria['price'] = ">= " . (int)$minPrice;
    $criteria['price'] = 'and, >= ' . $minPrice . ', <= ' . $maxPrice;
}


if($bedrooms) {
    $criteria['bedrooms'] = '>= ' . $bedrooms;
}


return [
    'endpoints' => [
        'property.json' => [
            'elementType' => Entry::class,
            'criteria' => $criteria,
            'paginate' => true, //!$showAll,
            'elementsPerPage' => 4,
            'pageParam' => 'page',
            'transformer' => function(Entry $entry) {
                $imager = new ImagerVariable();


                $images = $entry->images->all();
                $transformImages = [];
                foreach ($images as $key => $image) {
                    $transformImages [] = [
                        'tiny' => 'http://localhost/realproperties' . $imager->transformImage( $image , [ 'format' => 'jpg', 'width' => '200' ] )->getUrl(),
                        'placeholder' => 'http://localhost/realproperties' . $imager->transformImage( $image , [ 'format' => 'jpg', 'width' => '15', 'blur' => true ] )->getUrl(),
                        'url' => 'http://localhost/realproperties' . $imager->transformImage( $image , [ 'format' => 'jpg', 'width' => '400' ] )->getUrl()
                    ];
                    
                }
                return [
                    'id' => $entry->id,
                    'title' => $entry->title,
                    'url' => $entry->url,
                    'residentialStatus' => $entry->residentialStatus->all(),
                    // 'ref' => $entry->reference,
                    'description' => $entry->description,
                    'abstract' => $entry->abstract,
                   
                    'bathrooms' => $entry->bathrooms,
                    'bedrooms' => $entry->bedrooms,
                    'receptions' => $entry->receptions,
                    'petsWelcome'=>$entry->petsWelcome,
                    'parking'=>$entry->parking,
                    'garages'=>$entry->garages,

                    // 'latitude' => $entry->latitude,
                    // 'longitude' => $entry->longitude,
                    // 'sizeType' => $entry->sizeType,
                    // 'jsonUrl' => UrlHelper::url("news/{$entry->id}.json"),
                    'purchaseType' => $entry->purchaseType->all(),
                    
                    'parish' => $entry->parish->all(),
                    'price' => $entry->price,
                    
                    //'images' => $transformImages
                    'images' => $entry->images->all(),
                ];
            },
        ],
       
    ]
];